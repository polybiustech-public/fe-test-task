# Polybius front-end task

## Overview
Create a responsive single page application (SPA) where as a user I will be able to perform next tasks:
- Review expences by category (groceries, coffee, cinema, etc.). You can think of it as a dashboard.
- Select a category and go to category details page where expenses info will be displayed.

## Technologies stack

### Front-end
#### Language
- JavaScript (ES6)
- TypeSciprt instead of JavaScript would be a plus

#### Framework
- You can choose any of those React + Redux/Vue + Vuex/Angular

#### Styles
- CSS frameworks or just plain CSS - it is totally up to you
- postCSS/LESS/SASS would be a plus

#### Build tool
- To save your time we recommend to use CLI tools like: create-react-app, vue-cli, or @angular/cli
- If you want to build a configuration via Webpack by yourself it would be a plus, but it should not take a lot of time

#### Design
- Please check mockups where you can find a general design idea, but we encourage you to build something by yourself

### Backend
- Simple HTTP Node.js server (powered by express for example) in order to get data

## Data
- Please check data.json file

## Responsive requirements
- Would be a plus to create a mobile version (you can take your mobile phone resoluiton) along with desktop version which is a must.